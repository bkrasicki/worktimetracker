﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace WorkTimeTracker.Migrations
{
    public partial class AddWorkingTime : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<TimeSpan>(
                name: "WorkingTime",
                table: "EntryHistory",
                nullable: false,
                defaultValue: new TimeSpan(0, 0, 0, 0, 0));
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "WorkingTime",
                table: "EntryHistory");
        }
    }
}
