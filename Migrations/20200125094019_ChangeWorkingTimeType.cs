﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace WorkTimeTracker.Migrations
{
    public partial class ChangeWorkingTimeType : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<string>(
                name: "WorkingTime",
                table: "EntryHistory",
                nullable: true,
                oldClrType: typeof(TimeSpan),
                oldType: "time",
                oldNullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<TimeSpan>(
                name: "WorkingTime",
                table: "EntryHistory",
                type: "time",
                nullable: true,
                oldClrType: typeof(string),
                oldNullable: true);
        }
    }
}
