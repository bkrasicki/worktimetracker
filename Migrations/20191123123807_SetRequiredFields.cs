﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace WorkTimeTracker.Migrations
{
    public partial class SetRequiredFields : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "SurName",
                table: "Workers",
                newName: "Surname");

            migrationBuilder.AlterColumn<string>(
                name: "Surname",
                table: "Workers",
                nullable: false,
                oldClrType: typeof(string),
                oldType: "nvarchar(max)",
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "FirstName",
                table: "Workers",
                nullable: false,
                oldClrType: typeof(string),
                oldType: "nvarchar(max)",
                oldNullable: true);

            migrationBuilder.AddColumn<string>(
                name: "CardNumber",
                table: "Workers",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "WorkerId",
                table: "EntryHistory",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_EntryHistory_WorkerId",
                table: "EntryHistory",
                column: "WorkerId");

            migrationBuilder.AddForeignKey(
                name: "FK_EntryHistory_Workers_WorkerId",
                table: "EntryHistory",
                column: "WorkerId",
                principalTable: "Workers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_EntryHistory_Workers_WorkerId",
                table: "EntryHistory");

            migrationBuilder.DropIndex(
                name: "IX_EntryHistory_WorkerId",
                table: "EntryHistory");

            migrationBuilder.DropColumn(
                name: "CardNumber",
                table: "Workers");

            migrationBuilder.DropColumn(
                name: "WorkerId",
                table: "EntryHistory");

            migrationBuilder.RenameColumn(
                name: "Surname",
                table: "Workers",
                newName: "SurName");

            migrationBuilder.AlterColumn<string>(
                name: "SurName",
                table: "Workers",
                type: "nvarchar(max)",
                nullable: true,
                oldClrType: typeof(string));

            migrationBuilder.AlterColumn<string>(
                name: "FirstName",
                table: "Workers",
                type: "nvarchar(max)",
                nullable: true,
                oldClrType: typeof(string));
        }
    }
}
