﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WorkTimeTracker.Models
{
    public enum WorkerState
    {
        UnassignedCard = 0,
        NoAccess = 1,
        EntryOk = 2
    }
}
