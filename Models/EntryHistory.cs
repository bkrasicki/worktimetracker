﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace WorkTimeTracker.Models
{
    public class EntryHistory
    {
        public int Id { get; set; }

        [Required]
        public int Type { get; set; }

        [Required]
        public DateTime Date { get; set; }

        public string WorkingTime { get; set; }

        public Worker Worker { get; set; }
    }
}
