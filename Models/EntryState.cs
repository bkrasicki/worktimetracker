﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WorkTimeTracker.Models
{
    public enum EntryState
    {
        In = 0,
        Out = 1
    }
}
