﻿'use strict';

var temperatureSensor = {
    on: 1,
    off: 0
};

var connection;

function UI(hubConnection) {
    var self = this;
    var jq = $;
    var buttonSend = '#buttonSend';
    var tempStatus = '#tempStatus';

    this.loadEvents = function () {
        jq(buttonSend).click(self.sendMessageOnClick);
    };

    this.sendMessageOnClick = function () {
        var tempStatus = jq(tempStatus).is(':checked') === true ? temperatureSensor.on : temperatureSensor.off;
        console.log('Temp status = ' + tempStatus);
        hubConnection.invoke('ChangeStateMassage', tempStatus);
    };

    (function () {
        self.loadEvents();
    })();
}

(function () {
    connection = new signalR.HubConnectionBuilder()
        .withUrl("/temp-sensor")
        .configureLogging(signalR.LogLevel.Information)
        .build();

    connection.start().then(function () {
        console.log("connected");
    });
    var ui = new UI(connection);
})();

function clickButton() {
    var tempStatus = $('#tempStatus').is(':checked') === true ? temperatureSensor.on : temperatureSensor.off;
    console.log('Temp status = ' + tempStatus);
    connection.invoke('ChangeStateMassage', tempStatus);
}