﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using WorkTimeTracker.Models;

namespace WorkTimeTracker.Controllers
{
    public class WorkersController : Controller
    {

        private readonly ApplicationDbContext _context;

        public WorkersController(ApplicationDbContext context)
        {
            _context = context;
        }

        public IActionResult Index()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }

        public IActionResult Create()
        {
            return View("WorkerForm");
        }

        public IActionResult Edit(int id)
        {
            var worker = _context.Workers.SingleOrDefault(w => w.Id == id);

            if (worker == null)
                return NotFound();

            return View("WorkerForm", worker);
        }

        [HttpDelete]
        public IActionResult Delete(int id)
        {
            var worker = _context.Workers.SingleOrDefault(w => w.Id == id);

            if (worker == null)
                return NotFound();

            _context.Workers.Remove(worker);
            _context.SaveChanges();
            return Ok();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Save(Worker worker)
        {
            //if (!ModelState.IsValid)
            //{
            //    return View("WorkerForm", worker);
            //}

            if (worker.Id == 0)
            {
                _context.Workers.Add(worker);
            }
            else
            {
                var workerInDb = _context.Workers.SingleOrDefault(w => w.Id == worker.Id);
                workerInDb.FirstName = worker.FirstName;
                workerInDb.LastName = worker.LastName;
                workerInDb.HasAccess = worker.HasAccess;
                workerInDb.CardNumber = worker.CardNumber;
            }
            _context.SaveChanges();
            return RedirectToAction("Index", "Workers");
        }

        [HttpGet]
        public IEnumerable<Worker> GetWorkers()
        {
            var workers = _context.Workers.ToList();
            return workers;
        }

    }
}