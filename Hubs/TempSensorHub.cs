﻿using Microsoft.AspNetCore.SignalR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WorkTimeTracker.Models;

namespace WorkTimeTracker.Hubs
{
    public class TempSensorHub : Hub
    {
        
        public void ChangeStateMassage(EntryState state)
        {
            Clients.All.SendAsync("ChangeStateMassage", state);
        }

    }
}
